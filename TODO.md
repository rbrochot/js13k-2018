#Ideas
- how do we win the game?
- how do we signal game boundaries?
- add +/- sign on the phone if approach the signal or not
- Narration: monster sound, waking up, must call help, no signal
- make entities as primitives
- Use opacity animation to fade texts

#Build
- compare build results with/without helpers
- Include html generation in index.js? (for minification)

#Core
- Phone model OK
- Light from back of phone OK
- Signal origin generation OK
- Display signal force on phone OK
- Display text around phone OK
- Basic narration: find signal: call help, get response (in other color) OK
- Basic narration: Monster sound
- Basic narration: End at dawn (fade to black and "you survived" text)
- Basic narration: 911 Call triggers to time to survive => dawn and end
- Enemy: model
- Enemy: following
- Enemy: growling (sound) then attacking
- Enemy: fleeing from light
- Enemy: shrieking when fleeing

#Secondary
- vr controls for items (and in general)
- gamepad controls
- more realistic forest generations
- make so we cannot move through trees
- enemies and guns
- pick up items (and no items from start, flashlight in sight in the beginning)
- try to light phone without external light (threejs MeshBasicMaterial ?)
- various sounds: phone ring, human mumble, wind

#Esthetics & polish
- generate nightsky texture (stars...)
- generate herbs textures and herbs
- fallings leaves
- generate bushs

#sound
http://sfxr.me/
{
  "oldParams": true,
  "wave_type": 3,
  "p_env_attack": 0,
  "p_env_sustain": 0.4630729606243327,
  "p_env_punch": 0.5148477992864711,
  "p_env_decay": 0.446,
  "p_base_freq": 0.132,
  "p_freq_limit": 0.066,
  "p_freq_ramp": 0.004,
  "p_freq_dramp": -0.028,
  "p_vib_strength": 0.673,
  "p_vib_speed": 0.336,
  "p_arp_mod": -0.3162,
  "p_arp_speed": 0.7970067023197533,
  "p_duty": 0.11141812657668648,
  "p_duty_ramp": 0.45702401157197126,
  "p_repeat_speed": 0.028,
  "p_pha_offset": 0,
  "p_pha_ramp": 0,
  "p_lpf_freq": 1,
  "p_lpf_ramp": 0.12657001283500868,
  "p_lpf_resonance": 0.812153258720475,
  "p_hpf_freq": 0,
  "p_hpf_ramp": 0,
  "sound_vol": 0.068,
  "sample_rate": 44100,
  "sample_size": 8
}
