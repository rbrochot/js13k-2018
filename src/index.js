//Vendors
// import aframeEnvironment from 'aframe-environment-component';

import * as jsfxr from 'jsfxr';

//Helpers
import helpers from './helpers';

//Components
import forest from './components/forest';
import tree from './components/tree';
import player from './components/player';
import phone from './components/phone';

//Systems
import collisionDetection from './systems/collisionDetection';
import controller from './systems/controller';
import phoneSignal from './systems/phoneSignal';
import narration from './systems/narration';
