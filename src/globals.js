/* global AFRAME */
/* global THREE */

export const three = THREE;
export const aframe = AFRAME;
