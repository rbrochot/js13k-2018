export const WORLD_WIDTH = 200;
export const WORLD_DEPTH = 200;
export const TREE_RADIUS = 0.4;
export const TREE_MIN_HEIGHT = 15;
export const TREE_MAX_HEIGHT = 25;
export const TREE_NUMBER = 500;
export const PHONE_SIGNAL_REFRESH_RATE = 1500; //in ms
