import * as constants from '../constants';
import {aframe} from '../globals';

aframe.registerComponent('phone', {
	schema: {
	},

	init: function () {
		let phoneSignalSystem = document.querySelector('a-scene').systems['phoneSignalSystem'];
		let narrationSystem = document.querySelector('a-scene').systems['narrationSystem'];
		phoneSignalSystem.registerPhone(this);
		narrationSystem.registerPhone(this);
		this.el.setAttribute('visible', false); //Hidden at start, for narration

		this.case = document.createElement('a-box');
		this.case.setAttributes({
			'position': '0 0 0',
			'height': 0.2,
			'width': 0.1,
			'depth': 0.01,
			'color': '#000',
		});

		this.screen = document.createElement('a-box');
		this.screen.setAttributes({
			'position': '0 0 0.002',
			'height': 0.18,
			'width': 0.095,
			'depth': 0.01,
			'color': '#FFF',
		});
		this.screenNumber = document.createElement('a-text');
		this.screenNumber.setAttributes({
			value: 0,
			width: 0.1,
			'wrap-count': 1,
			color: '#000',
			position: '-0.05 0 0.007'
		});

		// Append to scene.
		this.el.appendChildren(this.case, this.screen, this.screenNumber);
	},
	show: function() {
		this.el.setAttribute('visible', true);
		//TODO add animation
	},
	light: function() {
		this.el.querySelector('#light').setAttribute('visible', true);
	},
	//function called by phoneSignalSystem to refresh screen with the current signal distance
	updateSignal: function (signalBars) {
		this.screenNumber.setAttribute('value', signalBars);
	},
});
