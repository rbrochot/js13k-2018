import {aframe} from '../globals';
import * as constants from '../constants';

aframe.registerComponent('tree', {
	schema: {
		height: {
			type: 'int',
			default: 10
		},
		foliageRadius: {
			type: 'number',
			default: 4
		},
		trunkRadius: {
			type: 'number',
			default: 0.5
		},
		position: {
			type: 'vec3',
		},
	},

	init: function () {
		this.trunk = document.createElement('a-cylinder');
		this.trunk.setAttributes({
			'position': [
				this.data.position.x,
				this.data.position.y + this.data.height / 2,
				this.data.position.z
			].join(' '),
			'segments-radial': 8,
			'height': this.data.height,
			'radius': this.data.trunkRadius,
			'color': '#7f3d03',
			'shadow': 'cast: true',
		});

		this.foliage = document.createElement('a-sphere');
		this.foliage.setAttributes({
			'position': [
				this.data.position.x,
				this.data.position.y + this.data.height,
				this.data.position.z
			].join(' '),
			'segments-height': 6,
			'segments-width': 6,
			'radius': this.data.foliageRadius,
			'color': '#00CC00',
		});

		// Append to scene.
		this.el.appendChildren(this.trunk, this.foliage);
	},
	tick: function(time, timeDelta) {
	},
});
