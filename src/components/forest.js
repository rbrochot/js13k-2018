import {aframe} from '../globals';
import * as constants from '../constants';

aframe.registerComponent('forest', {
	schema: {
	},

	init: function () {
		let tree, height, position, radius;
		let collisionDetectionSystem = document.querySelector('a-scene').systems['collisionDetectionSystem'];
		collisionDetectionSystem.registerForest(this);
		this.trees = [];

		for (var i = 0; i < constants.TREE_NUMBER; i++) {
			height = (Math.random() * (constants.TREE_MAX_HEIGHT - constants.TREE_MIN_HEIGHT)) + constants.TREE_MIN_HEIGHT;
			position = [
				(Math.random() * constants.WORLD_WIDTH - constants.WORLD_WIDTH / 2),
				0,
				(Math.random() * constants.WORLD_DEPTH - constants.WORLD_DEPTH / 2)
			];

			tree = document.createElement('a-entity');
			tree.setAttribute('tree', {
				position: position.join(' '),
				trunkRadius: constants.TREE_RADIUS,
				foliageRadius: 4,
				height: height,
			});
			// tree.setAttribute('position', position.join(' '));
			// tree.setAttribute('segments-radial', 8);
			// tree.setAttribute('height', height);
			// tree.setAttribute('radius', constants.TREE_RADIUS);
			// tree.setAttribute('color', '#7f3d03');
			// tree.setAttribute('shadow', 'cast: true');

			// tree = document.createElement('a-cylinder');
			// tree.setAttribute('position', position.join(' '));
			// tree.setAttribute('segments-radial', 8);
			// tree.setAttribute('height', height);
			// tree.setAttribute('radius', constants.TREE_RADIUS);
			// tree.setAttribute('color', '#7f3d03');
			// tree.setAttribute('shadow', 'cast: true');

			// Append to scene.
			this.el.appendChild(tree);
			this.trees.push(tree);
		}
	},
	tick: function(time, timeDelta) {
	}
});
