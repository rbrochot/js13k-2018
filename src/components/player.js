import {aframe} from '../globals';
import * as constants from '../constants';

aframe.registerComponent('player', {
	schema: {
	},

	init: function () {
		this.controllerSystem = document.querySelector('a-scene').systems['controllerSystem'];
		this.phoneSignalSystem = document.querySelector('a-scene').systems['phoneSignalSystem'];
		this.collisionDetectionSystem = document.querySelector('a-scene').systems['collisionDetectionSystem'];
		this.narrationSystem = document.querySelector('a-scene').systems['narrationSystem'];

		this.narrationSystem.registerPlayer(this);
		this.phoneSignalSystem.registerPlayer(this);
		this.collisionDetectionSystem.registerPlayer(this);
	},
	tick: function(time, timeDelta) {
	},
});
