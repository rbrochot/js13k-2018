import {aframe, three} from '../globals';
import * as constants from '../constants';

aframe.registerSystem('phoneSignalSystem', {
	schema: {
	},

	init: function () {
		let playerStartPosition = new three.Vector3(0, 0, 0);
		this.signalPosition = new three.Vector3(0, 0, -40); //Debug

		//Prod
		// do {
		// 	this.signalPosition.x = (Math.random() * constants.WORLD_WIDTH - constants.WORLD_WIDTH / 2);
		// 	this.signalPosition.y = (Math.random() * constants.WORLD_DEPTH - constants.WORLD_DEPTH / 2);
		// } while (this.signalPosition.distanceTo(playerStartPosition) < 75);

		this.distanceToPlayer = 0;
		this.totalDelta = 0;

		this.signalBars = null;
		this.newSignalBars = 0;

		this.signalFound = false;
	},
	tick: function(time, timeDelta) {
		this.totalDelta += timeDelta;
		if (this.totalDelta > constants.PHONE_SIGNAL_REFRESH_RATE) {
			this.totalDelta = 0;
			this.distanceToPlayer = this.signalPosition.distanceTo(this.player.el.object3D.position);
			this.newSignalBars = 5 - Math.min(5, Math.floor(this.distanceToPlayer / 15));
			if (this.newSignalBars !== this.signalBars) {
				this.signalBars = this.newSignalBars;
				this.phone.updateSignal(this.signalBars);
				if (!this.signalFound && this.signalBars === 5) {
					this.signalFound = true;
					document.querySelector('a-scene').systems['narrationSystem'].foundSignalEvent();
				}
			}
		}
	},
	registerPlayer: function(player) {
		this.player = player;
		this.tick(0, 10000);
	},
	registerPhone: function(phone) {
		this.phone = phone;
	},
});
