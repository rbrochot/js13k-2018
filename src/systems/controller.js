import {aframe} from '../globals';
aframe.registerSystem('controllerSystem', {
	schema: {
	},

	init: function () {
		this.moveKeys =  {
			up: false,
			down: false,
			left: false,
			right: false,
		};
		// document.addEventListener('keydown', this.onKeydown.bind(this));
		// document.addEventListener('keyup', this.onKeyup.bind(this));
	},
	tick: function() {
	},
	onKeydown: function(e) {
		switch (e.key) {
		case 'w':
		case 'z':
		case 'ArrowUp':
			this.moveKeys.up = true;
			break;
		case 's':
		case 'ArrowDown':
			this.moveKeys.down = true;
			break;
		case 'a':
		case 'q':
		case 'ArrowLeft':
			this.moveKeys.left = true;
			break;
		case 'd':
		case 'ArrowRight':
			this.moveKeys.right = true;
			break;
		}
	},
	onKeyup: function(e) {
		switch (e.key) {
		case 'w':
		case 'z':
		case 'ArrowUp':
			this.moveKeys.up = false;
			break;
		case 's':
		case 'ArrowDown':
			this.moveKeys.down = true;
			break;
		case 'a':
		case 'q':
		case 'ArrowLeft':
			this.moveKeys.left = false;
			break;
		case 'd':
		case 'ArrowRight':
			this.moveKeys.right = false;
			break;
		}
	},
});
