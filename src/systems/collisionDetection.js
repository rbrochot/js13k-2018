import {aframe} from '../globals';
import * as constants from '../constants';

aframe.registerSystem('collisionDetectionSystem', {
	schema: {
	},

	init: function () {
		this.distance = 0;
	},
	tick: function() {
		// if (typeof this.forest === 'undefined' || typeof this.player === 'undefined') { return; }
		// for (var i in this.forest.trees) {
		// 	if (this.forest.trees[i].isAlive) {
		// 		if (this.forest.trees[i].object3D.position.z > 0) {
		// 			//Tree has passed the player, not need to test anymore
		// 			this.forest.trees[i].isAlive = false;
		// 			continue;
		// 		}
		// 		//Deal with squared distance as it is less complex thant doing square root
		// 		this.distance = this.getSquaredDistanceBewteen(this.forest.trees[i].object3D, this.player.el.object3D);
		// 		if (this.distance < constants.TREE_RADIUS * constants.TREE_RADIUS) {
		// 			this.player.hit();
		// 			this.forest.trees[i].isAlive = false;
		// 		}
		// 	}
		// }
	},
	registerForest: function(forest) {
		this.forest = forest;
	},
	registerPlayer: function(player) {
		this.player = player;
	},
	getSquaredDistanceBewteen(obj1, obj2) {
		return Math.pow((obj1.position.x - obj2.position.x), 2) + Math.pow((obj1.position.z - obj2.position.z), 2);
	}
});
