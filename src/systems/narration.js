import {aframe} from '../globals';
import * as constants from '../constants';

aframe.registerSystem('narrationSystem', {
	schema: {
	},

	init: function () {
		this.textQueue = [];
		this.textEl = document.createElement('a-text');
		this.textEl.setAttributes({
			value: '',
			width: 0.5,
			anchor: 'center',
			'wrap-count': 40,
			color: '#FFF',
			position: '0 0.1 -0.3'
		});
	},
	tick: function(time, timeDelta) {
		if (this.textQueue.length > 0) {
			this.textQueue[0].duration -= timeDelta;
			if (this.textQueue[0].duration <= 0) {
				this.textQueue.shift();
				if (this.textQueue.length > 0) {
					this.textEl.setAttribute('value', this.textQueue[0].text);
					this.textEl.setAttribute('color', this.textQueue[0].color);
				}
				else {
					this.textEl.setAttribute('visible', false);
				}
			}
		}
	},
	registerPlayer: function(player) {
		player.el.appendChild(this.textEl);
	},
	registerPhone: function(phone) {
		this.phone = phone;
		//Start narration when when is registered
		this.startEvent();
	},
	showText: function (text, duration, color) {
		//Show text directly if no text is diplayed
		//TODO Mumbling sounds
		if (this.textQueue.length === 0) {
			this.textEl.setAttributes({
				'value': text,
				'visible': true,
				'color': color,
			});
		}
		this.textQueue.push({text, duration, color});
	},

	//Story events functions
	startEvent: function() {
		//TODO Monster sound
		this.showText('What was that noise?', 3000, '#FFF');
		this.showText('I really should call for help', 4000, '#FFF');
		this.showText('No signal... I should move around and try again', 4000, '#FFF');
		setTimeout(() => {
			this.phone.show();
		}, 5000);
		setTimeout(() => {
			this.phone.light();
		}, 8000);

		setTimeout(() => {
			this.showText('This was definitely not a good idea to go camp in the forest...', 4000, '#FFF');
		}, 20000);
	},
	foundSignalEvent: function() {
		this.showText('Ha! Full signal! I hope this works', 3000, '#FFF');
		setTimeout(() => {
			//TODO Dialing sound
		}, 2000);
		this.showText('911 here, how can I help you?', 3000, '#F0F');
		this.showText('I\'m lost in the forest, there was some strange roar', 3000, '#FFF');
		this.showText('You didn\'t know? That\'s the MONSTER' , 3000, '#F0F');
		this.showText('Don\'t worry we\'re sending help right now', 3000, '#F0F');
		this.showText('You should be safe now.', 3000, '#F0F');
	}
});
