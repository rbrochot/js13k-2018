
Element.prototype.setAttributes = function (attrs) {
	Object.keys(attrs).forEach(key => this.setAttribute(key, attrs[key]));
};

Node.prototype.appendChildren = function (...children) {
	children.forEach(child => this.appendChild(child));
};
