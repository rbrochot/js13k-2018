/* global __dirname */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackInlineSourcePlugin = require('html-webpack-inline-source-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const resolve = require('path').resolve;
const src = resolve(__dirname, 'src');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');

const isProduction = process.env.npm_lifecycle_event === 'build';

let htmlConfig = {
	inject: 'head',
	filename: 'index.html',
	template: 'src/index.html',
	minify: {
		removeComments: true,
		collapseWhitespace: true,
		collapseInlineTagWhitespace: true,
	}
};

if (isProduction) {
	htmlConfig.inlineSource = '.(js|css)$';
}

let config = {
	entry: './src/index.js',
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'script.js'
	},
	module: {
		rules: [
			{
				include: [ src ],
				exclude: /node_modules/,
				test: /\.js$/,
				enforce: 'pre',
				loader: 'eslint-loader',
				options: {
					configFile: './.eslintrc',
				},
			},
			{
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						// presets: [
						// 	['env', { 'modules': false }]
						// ]
						presets: [ 'babel-preset-es2015' ],
					}
				}
			},
			// {
			// 	test: /\.css$/,
			// 	use: ExtractTextPlugin.extract({
			// 		fallback: 'style-loader',
			// 		use: 'css-loader'
			// 	})
			// }
		]
	},
	plugins: [
		// new ExtractTextPlugin('style.css'),
		new HtmlWebpackPlugin(htmlConfig),
		new HtmlWebpackInlineSourcePlugin()
	],
	stats: 'minimal',
	devServer: {
		stats: 'minimal',
		port: 3000,
	}
};

if (!isProduction) {
	config.devtool = 'source-map';
	config.plugins.push(new OpenBrowserPlugin({
		url: 'http://localhost:3000/'
	}));
} else {
	config.plugins = config.plugins.concat([
		new webpack.optimize.ModuleConcatenationPlugin()
	]);
}

module.exports = config;
