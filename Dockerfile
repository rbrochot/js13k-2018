FROM node:8

# Create app directory
WORKDIR /app

# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
# COPY package.json ./

# If you are building your code for production
# RUN npm install --prod

# Bundle app source
COPY . .

# Install app dependencies
# RUN yarn install --prod
RUN yarn install

VOLUME /app/config

EXPOSE 3000
CMD [ "yarn", "start" ]
